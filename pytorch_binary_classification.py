import sys
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
from torch.utils.data import Dataset
from torchvision import transforms, datasets

batch_size = 8
root_dir = "animals"
img_size = 240
n_epochs = 30

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print("We're using =>", device)
print("The data lies here =>", root_dir)

train_root = root_dir + "/train"
test_root = root_dir + "/test"

train_transform = transforms.Compose([
    transforms.Resize((img_size, img_size)),
    transforms.ToTensor(),
    torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
])

test_transform = transforms.Compose([
    transforms.Resize((img_size, img_size)),
    transforms.ToTensor(),
    torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
])

# Creo dataset train e test alle immagini , applicando transformazione
train_data = datasets.ImageFolder(train_root, transform=train_transform)
test_data = datasets.ImageFolder(test_root, transform=test_transform)

# Data load , batch size di k immagini e rimescolaggio a true
train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True)
test_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size)


# Rete neurale
class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 6, 2)
        self.batch1 = nn.BatchNorm2d(6)
        self.pool1 = nn.MaxPool2d(4, 4)
        self.conv2 = nn.Conv2d(6, 12, 4)
        self.batch2 = nn.BatchNorm2d(12)
        self.pool2 = nn.MaxPool2d(4, 4)
        self.conv3 = nn.Conv2d(12, 24, 4)
        self.batch3 = nn.BatchNorm2d(24)
        self.pool3 = nn.MaxPool2d(4, 4)
        self.fc1 = nn.Linear(96, 1000)
        self.fc2 = nn.Linear(1000, 32)
        self.fc3 = nn.Linear(32, 2)

    def forward(self, x):
        x = self.pool1(F.mish(self.batch1(self.conv1(x))))
        x = self.pool2(F.mish(self.batch2(self.conv2(x))))
        x = self.pool3(F.mish(self.batch3(self.conv3(x))))
        x = torch.flatten(x, 1)  # flatten all dimensions except batch
        x = F.mish(self.fc1(x))
        x = F.mish(self.fc2(x))
        x = self.fc3(x)
        return x


net = Net()
net.to(device)
# Train del modello
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(net.parameters())

for epoch in range(n_epochs):
    print('Epoch {}/{}'.format(epoch, n_epochs - 1))
    net.train()
    running_loss = 0.0
    running_corrects = 0
    # Iterate over data.
    for inputs, labels in train_loader:
        inputs = inputs.to(device)
        labels = labels.to(device)
        # zero the parameter gradients
        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        _, preds = torch.max(outputs, 1)
        loss.backward()
        optimizer.step()
        # statistics
        running_loss += loss.item() * inputs.size(0)
        running_corrects += torch.sum(preds == labels.data)

    epoch_loss = running_loss / len(train_loader.dataset)
    epoch_acc = running_corrects.double() / len(train_loader.dataset)
    print('TRAIN Loss: {:.4f} Acc: {:.4f}'.format(epoch_loss, epoch_acc))

    running_loss = 0.0
    running_corrects = 0
    net.eval()
    # since we're not training, we don't need to calculate the gradients for our outputs
    with torch.no_grad():
        for data in test_loader:
            inputs, labels = data
            inputs = inputs.to(device)
            labels = labels.to(device)
            # calculate outputs by running images through the network
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            # the class with the highest energy is what we choose as prediction
            _, preds = torch.max(outputs.data, 1)
            running_loss += loss.item() * inputs.size(0)
            running_corrects += torch.sum(preds == labels.data)

    epoch_loss = running_loss / len(test_loader.dataset)
    epoch_acc = running_corrects.double() / len(test_loader.dataset)
    print('VALIDATION Loss: {:.4f} Acc: {:.4f}'.format(epoch_loss, epoch_acc))
